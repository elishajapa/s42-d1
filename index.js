// Using DOM
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

/* Alternative in retrieving an element - getElement
	document.getElementById('text-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByTagName('input');
*/

// interaction = event (click, keypress..)
// Event listener - an interaction b/w the user and the webpage
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

// Assign same event to multiple listeners

txtFirstName.addEventListener('keyup', (event) => {
	// Contains the element where the evemt happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
})